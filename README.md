# Kiesel Website

Simple website for the Kiesel JS engine, using Vue & Vite.

## Development

A few environment variables need to be set:

```text
VITE_KIESEL_DOCS_URL="..."
VITE_KIESEL_DOWNLOAD_URL="..."
VITE_KIESEL_SOURCE_URL="..."
VITE_KIESEL_WASM_URL="..."
```

Then, install dependencies and run the dev server:

```console
npm install
npm run dev
```

Or do a production build:

```console
npm build
```
