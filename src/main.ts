import { createApp } from "vue";
import { createRouter, createWebHistory } from "vue-router";
import "./style.css";
import App from "./App.vue";
import Index from "./views/Index.vue";
import Playground from "./views/Playground.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", component: Index },
    { path: "/playground", component: Playground },
  ],
});

const app = createApp(App);
app.use(router);
app.mount("#app");
