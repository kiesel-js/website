import { init as initWASI, WASI } from "@wasmer/wasi";

type RunResult = {
  durationMs: number;
  exitCode: number | null;
  stdout: string;
  stderr: string;
};

export class Kiesel {
  #wasmUrl: string;
  #wasmModule?: WebAssembly.Module;

  constructor(wasmUrl: string) {
    this.#wasmUrl = wasmUrl;
  }

  async init(): Promise<void> {
    await initWASI();
    this.#wasmModule = await WebAssembly.compileStreaming(fetch(this.#wasmUrl));
  }

  async run({
    source,
    args = [],
  }: {
    source: string;
    args?: string[];
  }): Promise<RunResult> {
    const wasi = new WASI({
      env: {},
      args: ["kiesel", "/file.js", ...args],
    });
    const module = this.#wasmModule!;
    const instance = await WebAssembly.instantiate(module, {
      ...wasi.getImports(module),
    });
    const file = wasi.fs.open("/file.js", {
      read: true,
      write: true,
      create: true,
    });
    file.writeString(source);
    const start = performance.now();
    try {
      const exitCode = wasi.start(instance);
      const durationMs = Math.round(performance.now() - start);
      return {
        durationMs,
        exitCode,
        stdout: wasi.getStdoutString(),
        stderr: wasi.getStderrString(),
      };
    } catch (error) {
      const durationMs = Math.round(performance.now() - start);
      return {
        durationMs,
        exitCode: null,
        stdout: "",
        stderr: `${(error as Error).message}\n${(error as Error).stack ?? ""}`,
      };
    }
  }
}
